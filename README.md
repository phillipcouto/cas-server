# CAS Server
This is a simple Content Addressable Storage server that exposes an API for
working with CAS objects.

The goal of this server is to provide the following operations:
* Create
* Read
* Delete
* Verify

To update a CAS object would be to create a new one as the checksum of the object
will be different. This server is meant to be part of a larger system or service
and not client facing. The server will also verify the contents of the objects
stored and can call a webhook when a corrupted CAS object is discovered.

Also the server will provide statistics like storaged used and storage available
so calling services can determine if the server should be used to store the
new object.

This project is currently in development and not building. Status updates
will be posted here.
